<p align="center">
<img src="images/présentation.jpeg">
</p>
# Mon parcours

Souvent apparenté à une fonction logarithmique, ou encore à une série française des _années 90’_, mon prénom aurait pu se composer de deux lettres **LN**. Vous l’aurez donc deviné [**je m’appelle Hélène**](https://www.youtube.com/watch?v=PyLrG0G5klA)
Ayant grandi en Belgique, dans une maison entourée de nature, j’ai d’abord suivi une formation plutôt scientifique jusqu’à la fin de mes secondaires. Les années passant, j’ai souvent bricolé avec mon papa en m’intéressant à chaque rénovation qu’il réalisait dans notre maison familiale. Chaque étape éveillant ma curiosité, je me suis alors dirigée vers des études d’architecture.  

Diplôme d’architecture en poche, j’ai décidé de compléter ma formation en m’inscrivant au master en sciences et gestion de l’environnement à l’ULB. Une formation complémentaire en dehors de mon domaine _« d’étude »_ qui m’a permis de réaliser pas mal de choses quant à notre modèle économique actuel et les dégâts toujours plus conséquents sur l’environnement. Aujourd’hui, je suis de plus en plus sensible à ces questions et notre mode de fonctionnement actuel se doit d’évoluer en tendant vers une certaine **résilience**.

Parallèlement, j’ai commencé à travailler au Fablab ULB en tant que _**fabmanageuse**_.  

Un an plus tard, me voilà prête à m’investir dans une nouvelle formation, Fabzéro version électro.
