# Documentation
## _Comment documenter ?_
Pour pouvoir réaliser une belle documentation, il faut installer :
* GIT
* BASH
* ATOM

Pour ce faire, plusieurs tutoriels sont à disposition à chaque étape d'installation...

_NB : Les tutoriels étant particulièrement clairs, je n'ai pas trouvé intéressant de documenter l’ensemble des étapes à franchir mais plutôt de compléter la documentation existante en fonction des problèmes que je rencontrais._

### Installer GIT/GITBASH

Pour commencer à documenter, il faut d'abord télécharger [GIT](https://git-scm.com/) et suivre les étapes de ce [tutoriel](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#install-git).
Cette étape de franchie, il faut maintenant installer BASH qui permet de travailler sous Linux à partir de windows. En démarrant l’installation à partir de ce [tutoriel]( https://korben.info/installer-shell-bash-linux-windows-10.html). J’ai suivi les étapes successives.


>_Le seul problème que j‘ai rencontré à  cette étape, c’est au moment où je dois trouver _bash_ sur mon ordinateur, le téléchargement ne se lance pas. Il faut donc aller directement dans la barre menu et se rendre sur **microsoft store**. Et, ensuite procéder manuellement au téléchargement de **Ubuntu**._

Cette étape de franchie, il ne reste plus qu'à continuer l'installation de BASH.

Enfin, on peut vérifier l'installation en ouvrant le terminal et en utilisant la commande suivante :
`git --version` et en ayant la réponse suivante `git version X.Y.Z`
Il faut ensuite configuer GITBASH avec quelques lignes de commandes que l'on retrouve [ici](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#install-git)

De la même manière, on peut effectuer une vérification après l'étape de la configuration passée. En tapant la ligne de commande suivante : `git config --global --list` et en s'attendant à avoir ce type de réponse `user.name=User_name` `user.email=email_adress@example.com`

### Installer ATOM

Pour plus de facilités, il est possible d'associer GITLAB à son propre ordinateur au lieu de le faire directement sur GITLAB.
Pour ce faire, il faut ajouter une clé SSH permettant la mise à disposition edu GITLAB sur notre ordinateur. J'ai choisi de travailler avec la clé `ED25519`.
Pour cela, rien de plus simple, il faut juste télécharger [ATOM](https://atom.io/) et procéder à l'installation.

D'ailleurs, pour cette étape j'ai vite bifurqué pour suivre les étapes de documentation de [Clémentine benyakhou](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/clementine.benyakhou/-/blob/master/docs/modules/module01.md) qui sont plus abordables pour les débutants ...

ATOM installé, le package du cours apparait sur la gauche de l'écran et chaque fichier est alors modifiable. Pour que les modifications soient visibles sur le GITLAB, il y a quelques étapes à franchir.
1. enregistrer ses modifications. (Cela fonctionne très bien en effectuant `ctrl+s`)
2. Cliquer sur Git en bas à droite. (Normalement si vous avez enregistrer votre fichier un chiffre apparait `(x)`)
3. Vérifier que les modifications apparaissent dans la catégorie `Unstaged Changes`.
4. Cliquer en haut à droite sur `Stage All`.
5. Valider en ajoutant un commentaire `Commit to master`
6. Et enfin, tout en bas sur la droite, cliquer sur `PUSH` et envoyer son travail sur GITLAB.

### Astuces

Les commandes "indispensables" pour écrire en markdown sont entre autre répertoriées sur ce [tutoriel](https://www.markdowntutorial.com/) intéractif.
Les bases connues, c'est parti pour le grand saut !

#### Nomenclature Markdown
[center]Texte centré[/center]

|   |  Markdown |
|:---:|:---|
| italique |  `_italique_`|
| gras  |  `**gras**` |
| gras italique | `**_gras italique_**` |
| Titre | `# titre` |
| Citation | `> text` |
| Liste non-ordonnée | `* text`|
| Liste ordonnée |`1. text`|
| Lien (en ligne)|`[Cliquez-ici](lien)`|
|Lien (en référence)|`[cliquez-ici](lien 1)` / `[cliquez-là](lien 2)`/ `[lien 1]=` / `[lien 2]=`|
|image (en ligne)|`[cliquez-ici](lien)`|
|image (en référence)|`[cliquez-ici](lien 1)`/ `[cliquez-là](lien 2)`/ `[lien 1]=` / `[lien 2]=`|

Certaines commandes (un peu plus complexes) que les commandes basiques peuvent être écrites en HTML et sont utilisables sur ATOM et permettent de compléter le markdown utilisé.

||Markdown|
|:---:|:---|
|centrer un texte|`[center]Texte centré[/center]`|
|centrer une image |`<p align="center"> <img src="..."> </p>`|
|justifier un texte |`<p style="text-align:justify;">Texte justifié :  ...</p>`|
|texte aligné à droite|`<div style="text-align:right;">Aligné à droite</div`|
|centrer un tableau|`<center>`|
|aligné le texte à gauche dans un tableau|`| :--------------- |`|
|centrer le texte dans un tableau|`|:---------------:|`|
|aligné le texte à droite dans un tableau|`| -----:|`|

##### _Comment diminuer la qualité des photos ?_
J'ai choisi de travailler à partir de GIMP (logiciel que je connaissais déjà). En travaillant avec GIT, il faut absolument diminuer la qualité des photos. Voici un [tutoriel](https://www.gimp.org/tutorials/GIMP_Quickies/#changing-the-size-filesize-of-a-jpeg) bien pratique.

##### _Comment mettre à jour son site web manuellement ?_
Il faut se rendre sur son projet sur GITLAB. ENsuite, sur la fenêtre verticale àgauche, se rendre dans l'onglet `CI/CD` et cliquer sur `Schedule`. Ensuite, se rendre sur l'onglet `Pipelines` où l'on peut voir quan dnotre site sera à jour.
