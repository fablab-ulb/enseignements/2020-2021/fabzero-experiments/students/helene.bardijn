# Conception assistée par ordinateur
Il existe une multitude de programmes permettant de dessiner sur ordinateur.  Personnellement, j’ai plutôt eu l’habitude d’utiliser autocad et sketchup pendant mes études. J’ai également découvert fusion 360 proposé par autodesk.
Ici, il est plutôt question d’utiliser des logiciels gratuits qui ne dépendent pas de licence payante :
* Openscad
* Freecad
### Openscad
La première étape à franchir c’est de télécharger le logiciel sur ce site.
Ensuite, il faut prendre possession des commandes et se lancer.
Sur l’image ci-dessous on voit l’interface d’openscad. Sur la gauche, on peut écrire les codes qui nous permettent de dessiner notre objet. Openscad fonctionne de manière différente par rapport aux autres programmes que j’ai déjà pu utiliser. Il faut décrire l’objet que l’on veut dessiner. Pour nous aider, [ce site](https://www.openscad.org/cheatsheet/) regroupe les commandes nous permettant de dessiner (à l’image d’une anti sèche).

Lorsque l'on dessine un objet sur openscad, il faut faire attention à la résolution. Par exemple, je dessine un cylindre
Sur Openscad, on dessine des formes simples que l’on peut ensuite fusionner.

#### Astuces
La touche `F5` permet de mettre à jour son dessin (compil).
OPENSCAD travaille toujours en **millimètre**.
##### _Comment réaliser des animations ?_
Il est également possible de réaliser des animations.
On va d’abord dessiner l’objet. Ensuite, se rendre dans l’onglet vues > animer et rentrer des coordonnées temporelles FPS et étapes.
Par exemple je vais noter FPS : 20 et étapes 180 ce qui nous permet de créer un gif.
##### _Comment ajouter des commentaires?_
Pour ajouter des commentaires ce qui peut être assez intéressant quand on enchaine les lignes de code `/* (...) */`.
##### _Comment centrer l'objet ?_
Il suffit d'ajouter  `center=true` dans la commande et l'objet se centrera.
######
### Freecad

_Note importante, il est possible de passer un fichier d’openscad vers freecad mais l’inverse est plutôt déconseillé._

Il faut commencer par télécharger Freecad.
